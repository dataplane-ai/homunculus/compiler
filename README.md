# The Compiler for the Homunculus Framework

This repository provides the source code for the Homunculus compiler.
The compiler takes as input the dataset and the Alchemy application (the model type and the optimization obejective), and generates the binary for the given underlying backend (along with its F1 score). 

> **Note:** This release is focused on single model apps like those found [here](https://gitlab.com/dataplane-ai/homunculus/applications). Multi-model features like chaining (sequential and parallel) and fusion are not supported in this release.

## Usage

### 1. Building the Homunculus compiler

> **Note:** these instructions have been tested with Ubuntu 20.04 LTS

- First setup the Python3 virtual environment:
```
$ python3 -m venv homunculus-venv
$ source homunculus-venv/bin/activate
```

> **Note:** to exit the virtual environment, type `deactivate`

- To install the compiler, run the following command (from within the virtual environment):
```
$ cd ~/compiler
$ make build
```

This will build and install the Homunculus compiler as a Python module.

### 2. Testing the Homunculus compiler

To verify whether the compiler is properly installed, run the following test (from within the virtual environment):

```
$ cd tests/simple-dnn
$ python main.py
```

It will compile the `simple-dnn` application and ouput the F1 score using a `dummy` backend.
```
Current optimum: 81.43473369041496
```

## Contact Us 
- [Tushar Swamy](https://www.linkedin.com/in/tushar-swamy-b4aa51b1/)
- [Annus Zulfiqar](https://annusgit.github.io/)
- [Muhammad Shahbaz](https://mshahbaz.gitlab.io/)
- [Kunle Olukotun](http://arsenalfc.stanford.edu/kunle/)
