# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import numpy as np
from .. algorithm import Algorithm
from ..metrics.metric import Metric
from sklearn.cluster import KMeans as sklKMeans


class KMeans(Algorithm):

    def __init__(self, model_name, metrics):
        self.model_name = model_name + "_KMeans"
        self.model = None
        self.metrics = metrics

    def getModel(self):
        return self.model

    def build(self, clusters, num_init, max_iters):
        model = sklKMeans(n_clusters=int(clusters), n_init=int(num_init), max_iter=int(max_iters), tol=0.000001, verbose=0)
        self.model = model

    def train(self, trainX, trainY):
        self.model.fit(trainX)

    def evaluate(self, testX, testY):
        y = self.model.predict(testX)
        predicted_labels = y
        true_labels = np.argmax(testY, 1)

        return Metric.get("vmeasure").getValue(true_labels, predicted_labels)

    @staticmethod
    def csOptimize(parameters, state):

        # parameters = inputs[0]
        # state = inputs[1]
        pars = []
        centroids = []

        classes = parameters["classes"]
        num_init = parameters["num_init"]
        max_iters = parameters["max_iters"]
        tnx = state["dataset"]["data"]["train"]
        tny = state["dataset"]["labels"]["train"]
        tsx = state["dataset"]["data"]["test"]
        tsy = state["dataset"]["labels"]["test"]
        kmeans = KMeans(state["name"], state["metrics"])
        kmeans.build(classes, num_init, max_iters)
        kmeans.train(tnx, tny)
        metric_value = kmeans.evaluate(tsx, tsy)

        metric_value = -1.0 * metric_value
        model = kmeans.getModel()

        del model
        del kmeans
        return metric_value, classes, centroids, pars
