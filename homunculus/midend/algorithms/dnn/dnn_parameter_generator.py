# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

from .. parameter_generator import ParameterGenerator


class DNNParameterGenerator(ParameterGenerator):

    def preInit(self):
        self.algorithm = "dnn"
        self.layer_iters = 2
        self.neuron_iters = 2

    def generateCombinedScenario(self, app_name):

        self.clearParameters()
        for key, val in self.defaults["parameters"].items():
            if val["optimize"] is True:
                if val["conditional"] is False:
                    self.addParameter(key, val["type"], val["range"],
                                      val["default"])
                else:
                    cond_key = val["conditional"]
                    cond_val = self.defaults["parameters"][cond_key]
                    for elem in range(cond_val["range"][0],
                                      cond_val["range"][1] + 1):
                        param_name = key + "_" + cond_key + "_" + str(elem)
                        self.addParameter(param_name, val["type"],
                                          val["range"], val["default"])

        return self.generateScenario(0, "", True, 0, app_name)

    def generatePhaseScenario(self, phase, model_arch, iter, app_name):

        if phase == "layer":
            return self.generateLayerScenario(model_arch, iter, app_name)

        elif phase == "neuron":
            return self.generateNeuronScenario(model_arch, iter, app_name)

        else:
            raise Exception("Unrecognized phase: " + str(phase))

    def generateLayerScenario(self, model_arch, iter, app_name):

        self.clearParameters()
        self.addParameter("layers", "integer", [1, 10], len(model_arch))
        self.addParameter("batch_size", "ordinal", [64, 128, 256], 256)
        self.addParameter("epochs", "integer", [1, 10], 10)

        return self.generateScenario(self.defaults["phases"]["layer"],
                                     "layer", True, iter, app_name)

    def generateNeuronScenario(self, model_arch, iter, app_name):

        self.clearParameters()
        for idx, val in enumerate(model_arch):
            self.addParameter("layer" + str(idx), "integer", [1, 10], val)

        self.addParameter("batch_size", "ordinal", [64, 128, 256], 256)
        self.addParameter("epochs", "integer", [1, 10], 10)

        return self.generateScenario(self.defaults["phases"]["neuron"],
                                     "neuron", True, iter, app_name)
