# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

# import random
import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.metrics import CategoricalAccuracy
from .. algorithm import Algorithm
from sklearn import metrics as sklmetrics
from ..metrics.metric import Metric
from tqdm.keras import TqdmCallback


class DNN(Algorithm):

    def __init__(self, model_name, arch, input_dim, output_dim, metrics):
        self.model_name = model_name + "_DNN"
        self.model = None
        self.arch = arch
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.metrics = metrics

    def getModel(self):
        return self.model

    def build(self):
        model = Sequential()
        # print(self.arch)
        # print(self.input_dim)
        # print(self.output_dim)
        model.add(keras.Input(shape=(self.input_dim,))) 
        for layer_idx, num_hidden_units in enumerate(self.arch):
            if num_hidden_units == 0:
                continue

            # if layer_idx == 0:
            #     model.add(Dense(num_hidden_units, input_dim=self.input_dim, activation='relu'))
            # else:
            #     model.add(Dense(num_hidden_units, activation='relu'))
            model.add(Dense(num_hidden_units, activation='relu'))

        model.add(Dense(self.output_dim, activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer='adam',
                      metrics=["accuracy"])
                      # metrics=[CategoricalAccuracy()])
                      # metrics=[tfa.metrics.F1Score(num_classes=5)])

        self.model = model

    def train(self, trainX, trainY, epochs, batch_size):
        # print(np.argmax(trainY, 1))
        print(self.arch)
        tnx = np.reshape(trainX, (len(trainX), len(trainX[0])))
        tny = np.reshape(trainY, (len(trainY), len(trainY[0])))
        self.model.fit(tnx, tny, epochs=epochs, batch_size=batch_size,
                       validation_split=0.1, verbose=0,
                       callbacks=[TqdmCallback(verbose=1)])
        # ty = self.model.predict(trainX)
        # y = self.model.predict(np.reshape(trainX, np.shape(trainX)))
        # print(np.argmax(y, 1))

    def evaluate(self, testX, testY):
        print("____________________________________________")
        # self.model.summary()
        y = self.model.predict(np.reshape(testX, np.shape(testX)))
        
        # print(y)
        predicted_labels = np.argmax(y, 1)
        # result = filter(lambda x: not(x==0), predicted_labels)
        true_labels = np.argmax(testY, 1)
        m = Metric.get("f1").getValue(true_labels, predicted_labels)
        
        # m = 100*sklmetrics.f1_score(true_labels, predicted_labels,
        #                            average="weighted")
        # print("++++++++++++++++++++++++++++++++++++++++++++++")
        print(np.unique(list(predicted_labels)))
        # print(list(true_labels))
        print(m)
        print("____________________________________________")


        return m

    @staticmethod
    def getOptimizer(state):

        def optimize(parameters):

            batch_size = parameters["batch_size"]
            epochs = parameters["epochs"]
            layers = []

            if (state["phase"] == "layer"):
                num_layers = parameters["layers"]
                hidden_units = 10
                layers = [hidden_units]*num_layers
                for i in range(min(len(state["arch"]), num_layers)):
                    layers[i] = state["arch"][i]

            elif (state["phase"] == "neuron"):
                for i in range(len(state["arch"])):
                    layers.append(parameters["layer" + str(i)])

            else:
                raise Exception("Unrecognized phase: " + str(state["phase"]))

            tnx = state["dataset"]["data"]["train"]
            tny = state["dataset"]["labels"]["train"]
            tsx = state["dataset"]["data"]["test"]
            tsy = state["dataset"]["labels"]["test"]
            dnn = DNN(state["name"], layers, state["input_dim"],
                      state["output_dim"], state["metrics"])
            print("\tBuilding model")
            dnn.build()
            print("\tTraining model")
            dnn.train(tnx, tny, epochs, batch_size)
            print("\tTesting model")
            metric_value = dnn.evaluate(tsx, tsy)

            model = dnn.getModel()
            arch = []
            for layer in model.layers:
                arch.append(len(layer.get_weights()[0][0]))

            print("\tChecking feasibility")
            valid = state["backend"].checkFeasibility({
                "name": state["name"] + "_" + str(state["global_counter"]),
                "model": model,
                "arch": arch,
                "metric_value": metric_value,
                "input_dim": state["input_dim"],
                "output_dim": state["output_dim"]})

            if valid and metric_value > state["optimum"]["metric_value"]:
                state["optimum"]["metric_value"] = metric_value
                state["optimum"]["model"] = dnn.getModel()

            state["plot"]["iter_value"].append(metric_value)
            state["plot"]["optimum_value"].append(
                    state["optimum"]["metric_value"])
            state["plot"]["valid"].append(valid)
            if state["counter"] < 11:
                state["plot"]["doe"].append("rs")
            else:
                state["plot"]["doe"].append("bo")

            state["counter"] = state["counter"] + 1
            state["global_counter"] = state["global_counter"] + 1

            output = {
                    state["metrics"][0]: -1.0 * metric_value,
                    "Valid": valid
            }

            return output

        optimize.__globals__["state"] = state
        return optimize

    @staticmethod
    def csOptimize(inputs):

        # pid = intuple[0]
        # inputs = intuple[1]
        # print(pid)

        parameters = inputs[0]
        state = inputs[1]

        batch_size = int(parameters["batch_size"])
        # epochs = int(parameters["epochs"])
        epochs = 10
        layers = []
        pars = []

        for key, val in parameters.items():
            if key.startswith("neurons_layers_"):
                layers.append(int(val))
                # elif key.startswith("pars_layers_"):
                pars.append(int(val))

        pars.append(state["output_dim"])
        # print("============================================")
        # print(layers)
        for idx, val in enumerate(layers):
            if val == 0:
                del layers[idx]
                del pars[idx]

        # print(layers)
        # print("============================================")
        tnx = state["dataset"]["data"]["train"]
        tny = state["dataset"]["labels"]["train"]
        tsx = state["dataset"]["data"]["test"]
        tsy = state["dataset"]["labels"]["test"]
#        print("============================================")
#        print(np.shape(tnx))
#        print(np.shape(tny))
#        print(tnx)
#        print(tny)
#        # print(tny)
#        print("============================================")
#
        dnn = DNN(state["name"], layers, state["input_dim"],
                  state["output_dim"], state["metrics"])
        dnn.build()
        dnn.train(tnx, tny, epochs, batch_size)
        metric_value = dnn.evaluate(tsx, tsy)

        metric_value = -1.0 * metric_value
        arch = []
        model = dnn.getModel()
        layers = []
        for layer in model.layers:
            if ((len(layer.get_weights()[1])) == 0):
                print(model)
            arch.append(len(layer.get_weights()[1]))
            layers.append(layer.get_weights())

        return metric_value, arch, layers, pars
