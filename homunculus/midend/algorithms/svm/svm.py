# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import numpy as np
from .. algorithm import Algorithm
from ..metrics.metric import Metric
from sklearn import svm


class SVM(Algorithm):

    def __init__(self, model_name, metrics):
        self.model_name = model_name + "_SVM"
        self.model = None
        self.metrics = metrics

    def getModel(self):
        return self.model

    def build(self):
        model = svm.SVC()
        self.model = model

    def train(self, trainX, trainY):
        train_labels = np.argmax(trainY, 1)
        self.model.fit(trainX, train_labels)

    def evaluate(self, testX, testY):
        y = self.model.predict(testX)
        predicted_labels = y
        true_labels = np.argmax(testY, 1)

        return Metric.get("f1").getValue(true_labels, predicted_labels)

    @staticmethod
    def csOptimize(inputs):

        parameters = inputs[0]
        state = inputs[1]
        pars = []

        tnx = state["dataset"]["data"]["train"]
        tny = state["dataset"]["labels"]["train"]
        tsx = state["dataset"]["data"]["test"]
        tsy = state["dataset"]["labels"]["test"]
        svm = SVM(state["name"], state["metrics"])

        print("Building SVM Model...")
        svm.build()
        print("Training SVM Model...")
        svm.train(tnx, tny)
        print("Testing SVM Model...")
        metric_value = svm.evaluate(tsx, tsy)

        metric_value = -1.0 * metric_value
        model = svm.getModel()
        weights = []
        return metric_value, weights, pars
