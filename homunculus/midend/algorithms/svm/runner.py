# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import sys
import csv
import numpy as np
from os import path
from colorama import Fore
from collections.abc import Mapping
from homunculus.utils.registry import Registry
from homunculus.utils.helpers import csHypermapper
from .. runner import Runner as RunnerClass
from. svm import SVM
from. svm_parameter_generator import SVMParameterGenerator


class Runner(Registry, RunnerClass):

    registry_name = "svm"
    defaults_list = {
            "num_iters": 1,
            "hypermapper": {
                    "parameters": {
                        "kernel": {
                            "range": [0, 3],
                            "default": 0,
                            "type": "integer",
                            "optimize": True,
                            "conditional": False,
                        },
                        "pars": {
                            "range": [1, 10],
                            "default": 1,
                            "type": "integer",
                            "optimize": True,
                            "conditional": False
                        }


                    },
                    "iterations": 10,
                    "evals_per_iter": 1,
                    "rs": 5,
                    "mode": "client-server"
            },
            "state": {
                    "name": None,
                    "dataset": None,
                    "backend": None,
                    "metrics": ["accuracy"],
                    "metric_value": 0,
                    "plot": {
                            "iter_value": [],
                            "optimum_value": [],
                            "valid": [],
                            "doe": []
                    },
                    "optimum": {
                            "metric_value": -99999,
                            "model": None
                    },
            }
    }

    def __init__(self):
        return

    def checkData(self, labelled_data):

        if not(isinstance(labelled_data, Mapping)):
            raise Exception("Expected mapping object for data and labels")

        if not("data" in labelled_data.keys()):
            raise Exception("Expected field \"data\" in return value " +
                            "from data loader function")

        if not("labels" in labelled_data.keys()):
            raise Exception("Expected field \"labels\" in return " +
                            "value from data loader function")

        labelled_data["data"]["train"] = np.array(
                                        labelled_data["data"]["train"],
                                        dtype="float32")
        labelled_data["labels"]["train"] = np.array(
                                        labelled_data["labels"]["train"],
                                        dtype="float32")
        labelled_data["data"]["test"] = np.array(
                                        labelled_data["data"]["test"],
                                        dtype="float32")
        labelled_data["labels"]["test"] = np.array(
                                        labelled_data["labels"]["test"],
                                        dtype="float32")

        tnx_shape = np.shape(labelled_data["data"]["train"])
        tny_shape = np.shape(labelled_data["labels"]["train"])

        input_dim = tnx_shape[1] if len(tnx_shape) == 2 else 1
        output_dim = tny_shape[1] if len(tny_shape) == 2 else 1

        defaults = self.getDefaults()
        defaults["state"]["input_dim"] = input_dim
        defaults["state"]["output_dim"] = output_dim

        return labelled_data

    def run(self, app_name, metrics, load_func, backend_runner):

        filename = path.splitext(sys.argv[0])[0]
        scenario_dir = filename + "_metadata/scenarios"
        defaults = self.getDefaults()
        hm_defaults = defaults["hypermapper"]
        spg = SVMParameterGenerator(app_name, metrics, scenario_dir,
                                    hm_defaults)

        user_state = {
                            "name": app_name + "_svm",
                            "dataset": self.checkData(load_func()),
                            "backend": backend_runner,
                            "metrics": metrics,
        }
        state = dict(defaults["state"], **user_state)

        print(Fore.CYAN + "Running svm with metric \"" + str(metrics) +
              "\" for " + str(defaults["num_iters"]) + " iterations")

        param_file, io_dict = spg.generateCombinedScenario(state["name"])
        csHypermapper(param_file, io_dict, SVM.csOptimize, state)

        print(Fore.MAGENTA + "Current optimum: " +
              str(state["optimum"]["metric_value"]) + Fore.CYAN)

        with open(app_name + ".csv", 'w+') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=",", quotechar='|',
                                    quoting=csv.QUOTE_MINIMAL)

            row = ["iter", "iter_value", "optimum_value", "valid", "doe"]
            spamwriter.writerow(row)
            plot = state["plot"]
            for idx, iter_val in enumerate(plot["iter_value"]):
                row = [str(idx), str(iter_val),
                       str(plot["optimum_value"][idx]),
                       str(plot["valid"][idx]), str(plot["doe"][idx])]
                spamwriter.writerow(row)

        print(Fore.GREEN)
        return {
                "name": state["name"],
                "algorithm": "svm",
                "metric_value": state["optimum"]["metric_value"],
                "arch": state["optimum"]["arch"],
                "input_dim": state["input_dim"],
                "output_dim": state["output_dim"],
                "model": state["optimum"]["model"],
                "pars": state["optimum"]["pars"]
        }
