# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

from sklearn import metrics
import numpy as np


class Metric:

    @classmethod
    def get(cls, name):
        if name == "accuracy":
            return Accuracy

        elif name == "precision":
            return Precision

        elif name == "recall":
            return Recall

        elif name == "f1":
            return F1

        elif name == "vmeasure":
            return VMeasureScore


class Accuracy():

    @classmethod
    def getValue(self, true_labels, predicted_labels):
        return 100*metrics.accuracy_score(true_labels, predicted_labels)


class Precision():

    @classmethod
    def getValue(self, true_labels, predicted_labels):
        return 100*metrics.precision_score(true_labels, predicted_labels)


class Recall():

    @classmethod
    def getValue(self, true_labels, predicted_labels):
        return 100*metrics.recall_score(true_labels, predicted_labels)


class F1():

    @classmethod
    def getValue(self, true_labels, predicted_labels):
        return 100*metrics.f1_score(true_labels, predicted_labels,
                                    average="weighted",
                                    labels=np.unique(predicted_labels))


class VMeasureScore():

    @classmethod
    def getValue(self, true_labels, predicted_labels):
        return 100*metrics.cluster.v_measure_score(true_labels,
                                                   predicted_labels)
