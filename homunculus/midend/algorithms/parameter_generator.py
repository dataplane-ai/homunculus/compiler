# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import json
import os


class Parameter:
    def __init__(self, param_name, param_type, values, param_default):
        self.name = param_name
        self.dict = {
                    'parameter_type': param_type,
                    'values': values,
                    'parameter_default': param_default
        }


class ParameterGenerator:

    def __init__(self, name, objectives, scenario_dir, defaults):

        self.preInit()
        self.name = name
        self.objectives = objectives
        self.parameters = []
        self.scenario_dir = scenario_dir
        self.defaults = defaults
        os.makedirs(self.scenario_dir, exist_ok=True)

    def addParameter(self, pname, ptype, pvalues, pdefault):
        self.parameters.append(Parameter(pname, ptype, pvalues, pdefault))

    def clearParameters(self):
        self.parameters = []

    def preInit(self):
        self.algorithm = ""
        return

    def generateScenario(self, iterations, phase="", feasibility=False,
                         iter=0, app_name=""):

        self.filename = (self.name + "_" + self.algorithm + phase +
                         "_scenario.json")
        self.filepath = self.scenario_dir + "/" + self.filename

        if os.path.isfile(self.filepath):
            os.remove(self.filepath)

        param_dict = {param.name: param.dict for param in self.parameters}
        scenario = {
                'application_name': app_name,
                'optimization_objectives': self.objectives,
                'optimization_iterations': self.defaults["iterations"],
                "resume_optimization": False,
                "resume_optimization_data": "/home/tushar/homunculus-compiler/anomaly_detection_dnn_output_samples.csv",
                "evaluations_per_optimization_iteration": self.defaults[
                        "evals_per_iter"],
                'input_parameters': param_dict,
                "hypermapper_mode": {
                        "mode": self.defaults["mode"]
                },
                "design_of_experiment": {
                        "doe_type": "random sampling",
                        "number_of_samples": self.defaults["rs"]
                }
        }

        if feasibility:
            feasiblity_dict = {
                    "enable_feasible_predictor": True,
                    "name": "Valid",
                    "true_value": "True",
                    "false_value": "False"
            }

            scenario["feasible_output"] = feasiblity_dict

        with open(self.filepath, 'wt') as jf:
            json.dump(scenario, jf, indent=4)

        io_dict = {
                "inputs": [param.name for param in self.parameters],
                "outputs": self.objectives
        }

        return os.path.abspath(self.filepath), io_dict
