# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import json
import os
import sys
import dill


class MetaParser:

    def __init__(self):

        self.filename = os.path.splitext(sys.argv[0])[0]
        self.metadir = self.filename + "_metadata"
        self.path = self.metadir + "/" + self.filename
        self.json_ext = ".json"
        self.loader_ext = "_loaders.bin"

    def createMetadataFromPlatform(self, platform, filename_addition):

        os.makedirs(self.metadir, exist_ok=True)
        self.json_file = self.path + filename_addition + self.json_ext
        self.loader_file = self.path + filename_addition + self.loader_ext
        platform.writeMetadata(self.json_file, self.loader_file)

    def readMetadata(self):

        json_dict = None
        loader_dict = None

        with open(self.json_file, "rt") as jf:
            json_dict = json.load(jf)

        with open(self.loader_file, "rb") as lf:
            loader_dict = dill.load(lf)

        for name, model in json_dict["models"].items():

            func_name = model["data_loader"]
            model["data_loader"] = loader_dict[func_name]

        return json_dict
