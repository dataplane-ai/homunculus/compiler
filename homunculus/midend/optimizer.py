# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

from homunculus.utils.registry import Registry


class Optimizer:

    @classmethod
    def run(cls, program):

        backend = Registry.get("backends/supported/" + program["platform name"])
        optimal_algo_and_models = {}

        for model_name, model in program["models"].items():
            loader = model["data_loader"]
            metrics = model["optimization_metric"]

            optimal_models = []
            for algo_name in model["algorithm"]:
                algo_config = {}
                if algo_name in model["config"]:
                    algo_config = model["config"][algo_name]

                algo_runner = Registry.get("algorithms/supported/" +
                                           algo_name + "/runner")()
                be_runner = backend["runner"](algo_name,
                                              program["constraints"],
                                              program["resources"])
                m = algo_runner.run(model_name, metrics, loader, be_runner,
                                    algo_config)
                m["runner"] = be_runner
                optimal_models.append(m)

            best_model = max(optimal_models, key=lambda m: m["metric_value"])
            optimal_algo_and_models[model_name] = best_model
            if best_model["model"] is None:
                print("Optimum model not found")
            #else:
            #    best_model["runner"].checkFeasibilityPar([best_model], True, True)

        # entry["optimal_model"] =
        # best_entry = max(self.algo_entries, key=lambda
        # entry:entry["optimal_model"]["metric"])
