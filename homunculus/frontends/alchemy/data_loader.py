# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import types
import functools
import inspect
import importlib

class DataLoader:

    def __init__(self, user_func):

        self.global_vars = {}
        self.imports = {}

        functools.update_wrapper(self, user_func)
        self._saveGlobals(user_func)
        self.load_data = user_func

        self.name = user_func.__name__

        
    def _saveGlobals(self, func):
        func_globals = inspect.getclosurevars(func).globals
        
        for key,val in func_globals.items():
            if isinstance(val, types.ModuleType):
                self.imports[key] = val.__name__

            elif isinstance(val, types.FunctionType):
                self._saveGlobals(val)
                self.global_vars[key] = val

            else:
                self.global_vars[key] = val
 
    def getName(self):
        return self.name

    def __call__(self):

        for key,val in self.imports.items():
            self.load_data.__globals__[key] = importlib.import_module(val)

        for key,val in self.global_vars.items(): 
            self.load_data.__globals__[key] = val

        return self.load_data()
        
    #def serialize(self):
    #    return codecs.encode(dill.dumps(self), "base64").decode()

