# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import abc
from warnings import warn

class Recipe(abc.ABC):

    def __init__(self, userObj = {}):

        self.preInit(userObj)

        defaultObj = self.getDefaultInit()
        
        self.initWarning(defaultObj, userObj)

        mergeObj = dict(defaultObj, **userObj)

        self.postInit(mergeObj)


    @abc.abstractmethod
    def getDefaultInit(self):
        pass
    
    @abc.abstractmethod
    def preInit(self, user):
        pass

    @abc.abstractmethod
    def postInit(self):
        pass
    

    def initWarning(self, default, user):
        diff = set(user) - set(default)
        if not(len(diff) == 0):
            warn(self.warnStr + str(diff), stacklevel=self.warn_stacklevel)


    def getObj(self):
        return self.mainObj


