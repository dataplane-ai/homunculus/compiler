import json
import abc
from warnings import warn
import dill
import networkx as nx
import sys
import os
import re
import codecs
import __main__
import findimports as fi
import types
import functools
import inspect
import importlib as il

class HomunculusObject(abc.ABC):

    def __init__(self, userObj = {}):

        self.preInit(userObj)

        defaultObj = self.getDefaultInit()
        
        self.initWarning(defaultObj, userObj)

        mergeObj = dict(defaultObj, **userObj)

        self.postInit(mergeObj)


    @abc.abstractmethod
    def getDefaultInit(self):
        pass
    
    @abc.abstractmethod
    def preInit(self, user):
        pass

    @abc.abstractmethod
    def postInit(self):
        pass
    

    def initWarning(self, default, user):
        diff = set(user) - set(default)
        if not(len(diff) == 0):
            warn(self.warnStr + str(diff), stacklevel=self.warn_stacklevel)


    def getObj(self):
        return self.mainObj


class Platform(HomunculusObject):


    def getDefaultInit(self):
        return {}

    def preInit(self, userObj):
        self.jsonID = "platform"
        
        self.warnStr = "Unspecified resources found for platform \"" + self.getPlatformName() + "\": ",
        self.warn_stacklevel=3

        self.mainObj = {
                "platform name" : self.getPlatformName(),
        }
    
        return



    def getPlatformName(self):

        return "UNDEFINED"


    def setPlatformName(self, name):
    
        self.mainObj["name"] = name

        return


    def postInit(self, initResources):
        
        self.mainObj["resources"] = initResources
        self.mainObj["constraints"] = self.getDefaultConstraints()
        

    def getDefaultConstraints():
        return {}

    def constrain(self, userConstraints = {}):
        
        defaultConstraints = self.getDefaultConstraints()
        
        newConstraints = set(userConstraints) - set(defaultConstraints) 

        if not(len(newConstraints) == 0):
            warn("Unspecified constraints found for platform \"" + self.getPlatformName() + "\": " + str(newConstraints), stacklevel=2)

        constraints = dict(defaultConstraints, **userConstraints)

        self.mainObj["constraints"] = constraints
        
        return 


    def schedule(self, model_schedule):
        


        model_dict = {}
        for model_name, model in model_schedule.model_set.items():
            model_obj = model.getObj()
            del model_obj["name"]
            model_dict[model_name] = model_obj

        self.mainObj["models"] = model_dict

        nld = nx.json_graph.node_link_data(model_schedule.model_graph)
        self.mainObj["schedule"] = nld


    def writeJSON(self):
        filename_ext = sys.argv[0]
        filename = os.path.splitext(filename_ext)[0]
        filename = filename + ".json"
        IRFile = open(filename, "w") 

        json.dump(self.mainObj, IRFile, indent = 4) 

        IRFile.close() 


class Taurus(Platform):

    def getPlatformName(self):
        return "Taurus"

    def getDefaultInit(self):
        return {
                "rows" : 8,
                "cols" : 8,
                "preMATs" : "UNDEFINED",
                "postMATs" : "UNDEFINED",
                "grid_pattern" : "UNDEFINED"
        }


    def getDefaultConstraints(self):
        return {
                "throughput" : 1,
                "throughput_unit": "GigaPkts/sec",
                "latency" : "UNDEFINED",
                "latency_unit" : "nanoseconds"
        }
    



class Model(HomunculusObject):

    def getDefaultInit(self):
 
        return {
                "name" : "UNDEFINED",
                "optimization_metric" : "UNDEFINED",
                "input_map" : "UNDEFINED",
                "output_map" : "UNDEFINED",
                "algorithm" : "UNDEFINED",
                "external_model" : "UNDEFINED",
                "data_loader" : "UNDEFINED"
        }


    def preInit(self, userObj):
        
        model_name = "UNDEFINED"
        if "name" in userObj.keys():
            model_name = userObj["name"]

        self.warnStr = "Unspecified parameters found for model \"" + model_name + "\": "
        self.warn_stacklevel = 3

        if "data_loader" in userObj.keys():
                
            if not isinstance(userObj["data_loader"], Loader):
                raise Exception("Parameter \"data_loader\" must of type \"Loader\"")
            
            userObj["data_loader"] = codecs.encode(dill.dumps(userObj["data_loader"]), "base64").decode()

        
        return

    def postInit(self, initParams):
    
        self.mainObj = initParams
        return 





    def optimize(metric):
        
        self.mainObj["optimization_metric"] = metric
        
        return


    def __or__(self, model2):

        ms1 = ModelSchedule(self)

        ms2 = None
        if type(model2) is Model:
            ms2 = ModelSchedule(model2)

        elif type(model2) is ModelSchedule:
            ms2 = model2

        else:
            raise Exception("Arguments must be a Model or Model Schedule")

        ms = ms1 | ms2
        return ms

        
    def __gt__(self, model2):

        ms1 = ModelSchedule(self)

        ms2 = None
        if type(model2) is Model:
            ms2 = ModelSchedule(model2)

        elif type(model2) is ModelSchedule:
            ms2 = model2

        else:
            raise Exception("Arguments must be a Model or Model Schedule")

        ms = ms1 > ms2
        return ms




class ModelSchedule:


    def __init__(self, model):

        model_name = model.getObj()["name"]
        self.model_set = {model_name : model}

        self.model_graph = nx.DiGraph()
        self.model_graph.add_node(model_name)


    def __or__(self, ms2):
        self.model_set.update(ms2.model_set)


        self.model_graph = nx.compose(self.model_graph, ms2.model_graph)


        return self


    def __gt__(self, ms2):
        self.model_set.update(ms2.model_set)
        

        sink_nodes = self.getSinkNodes()
        source_nodes = ms2.getSourceNodes()


        for node1 in sink_nodes:
            for node2 in source_nodes:
                self.model_graph.add_edge(node1, node2)

        return self



    def getSourceNodes(self):
        source_nodes = []
        for node in self.model_graph.nodes:
            in_degree = self.model_graph.in_degree(node)
            if in_degree == 0:
                source_nodes.append(node)

        return source_nodes



    def getSinkNodes(self):
 
        sink_nodes = []
        for node in self.model_graph.nodes:
            out_degree = self.model_graph.out_degree(node)
            if out_degree == 0:
                sink_nodes.append(node)

        return sink_nodes



class Loader:

    def __init__(self, user_func=None):

        def default_load():
            pass

        self.load_data = default_load
        self.global_vars = {}
        self.imports = {}

        if user_func is not None:
            functools.update_wrapper(self, user_func)
            self._saveGlobals(user_func)
            self.load_data = user_func


        
    def _saveGlobals(self, func):
        func_globals = inspect.getclosurevars(func).globals
        
        for key,val in func_globals.items():
            if isinstance(val, types.ModuleType):
                self.imports[key] = val.__name__

            elif isinstance(val, types.FunctionType):
                self._saveGlobals(val)
                self.global_vars[key] = val

            else:
                self.global_vars[key] = val
    

    def load(self):

        for key,val in self.imports.items():
            self.load_data.__globals__[key] = il.import_module(val)

        for key,val in self.global_vars.items(): 
            self.load_data.__globals__[key] = val

        return self.load_data()
        



