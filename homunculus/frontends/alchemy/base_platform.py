# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import json
import abc
import sys
import os
import codecs
import dill
import networkx as nx
from .recipe import Recipe
from .model import Model, ModelSchedule

class Platform(Recipe, abc.ABC):


    def preInit(self, userObj):
        self.jsonID = "platform"
        
        self.warnStr = "Unspecified resources found for platform \"" + self.getPlatformName() + "\": ",
        self.warn_stacklevel=3

        self.mainObj = {
                "platform name" : self.getPlatformName().lower(),
        }
    
        return


    @abc.abstractmethod
    def getDefaultResources(self):
        pass

    @abc.abstractmethod
    def getPlatformName(self):
        pass

    @abc.abstractmethod
    def getDefaultConstraints(self):
        pass


    def getDefaultInit(self):
        return self.getDefaultResources()



    def postInit(self, initResources):
        
        self.mainObj["resources"] = initResources
        self.mainObj["constraints"] = self.getDefaultConstraints()
        

    def constrain(self, userConstraints = {}, userResources = {}):
        
        defaultConstraints = self.getDefaultConstraints()
        defaultResources = self.getDefaultResources()
        
        newConstraints = set(userConstraints) - set(defaultConstraints) 
        newResources = set(userResources) - set(defaultResources) 

        if not(len(newConstraints) == 0):
            warn("Unspecified constraints found for platform \"" + self.getPlatformName() + "\": " + str(newConstraints), stacklevel=2)

        constraints = dict(defaultConstraints, **userConstraints)
        resources = dict(defaultResources, **userResources)

        self.mainObj["constraints"] = constraints
        self.mainObj["resources"] = resources
        
        return self


    def schedule(self, model_input):
 
        model_schedule = None
        if type(model_input) is Model:
            model_schedule = ModelSchedule(model_input)
        
        elif type(model_input) is ModelSchedule:
            model_schedule = model_input

        else:
            raise Exception("Input to schedule must be of type Model or ModelSchedule")



        model_dict = {}
        self.loader_dict = {}

        for model_name, model in model_schedule.model_set.items():
            
            model_obj = model.getObj()
            del model_obj["name"]
            model_dict[model_name] = model_obj

            self.loader_dict[model.loader.getName()] = model.loader


        self.mainObj["models"] = model_dict

        nld = nx.json_graph.node_link_data(model_schedule.model_graph)
        self.mainObj["schedule"] = nld

        return self


    def writeMetadata(self, json_file, func_file):

        with open(json_file, "wt") as jf: 
            json.dump(self.mainObj, jf, indent = 4) 

        with open(func_file, "wb") as ff:
            dill.dump(self.loader_dict, ff)

    def getProgram(self):

        for name,model in self.mainObj["models"].items():
            
            func_name = model["data_loader"]
            model["data_loader"] = self.loader_dict[func_name]

        
        return self.mainObj

        


