# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import networkx as nx
from .recipe import Recipe
from .data_loader import DataLoader


class Model(Recipe):

    def getDefaultInit(self):

        return {
                "name": "UNDEFINED",
                "optimization_metric": "UNDEFINED",
                "input_map": "UNDEFINED",
                "output_map": "UNDEFINED",
                "algorithm": "UNDEFINED",
                "external_model": "UNDEFINED",
                "data_loader": "UNDEFINED",
                "config": {}
        }

    def preInit(self, userObj):

        model_name = "UNDEFINED"
        if "name" in userObj.keys():
            model_name = userObj["name"]

        self.warnStr = "Unspecified parameters found for model \"" + \
            model_name + "\": "
        self.warn_stacklevel = 3

        if "data_loader" in userObj.keys():
            loader = userObj["data_loader"]

            if not isinstance(loader, DataLoader):
                raise Exception("Data_loader not type \"Loader\"")

            userObj["data_loader"] = loader.getName()
            self.loader = loader

        return

    def postInit(self, initParams):

        self.mainObj = initParams

        return

    def __or__(self, model2):

        ms1 = ModelSchedule(self)

        ms2 = None
        if type(model2) is Model:
            ms2 = ModelSchedule(model2)

        elif type(model2) is ModelSchedule:
            ms2 = model2

        else:
            raise Exception("Arguments must be a Model or Model Schedule")

        ms = ms1 | ms2
        return ms

    def __gt__(self, model2):

        ms1 = ModelSchedule(self)

        ms2 = None
        if type(model2) is Model:
            ms2 = ModelSchedule(model2)

        elif type(model2) is ModelSchedule:
            ms2 = model2

        else:
            raise Exception("Arguments must be a Model or Model Schedule")

        ms = ms1 > ms2
        return ms


class ModelSchedule:

    def __init__(self, model):

        model_name = model.getObj()["name"]
        self.model_set = {model_name: model}

        self.model_graph = nx.DiGraph()
        self.model_graph.add_node(model_name)

    def __or__(self, ms2):
        self.model_set.update(ms2.model_set)

        self.model_graph = nx.compose(self.model_graph, ms2.model_graph)

        return self

    def __gt__(self, ms2):
        self.model_set.update(ms2.model_set)

        sink_nodes = self.getSinkNodes()
        source_nodes = ms2.getSourceNodes()

        for node1 in sink_nodes:
            for node2 in source_nodes:
                self.model_graph.add_edge(node1, node2)

        return self

    def getSourceNodes(self):
        source_nodes = []
        for node in self.model_graph.nodes:
            in_degree = self.model_graph.in_degree(node)
            if in_degree == 0:
                source_nodes.append(node)

        return source_nodes

    def getSinkNodes(self):

        sink_nodes = []
        for node in self.model_graph.nodes:
            out_degree = self.model_graph.out_degree(node)
            if out_degree == 0:
                sink_nodes.append(node)

        return sink_nodes
