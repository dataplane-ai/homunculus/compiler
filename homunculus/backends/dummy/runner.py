# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import os
import importlib
from homunculus.backends.registry import BackendRegistry
from homunculus.backends.backend import Backend


class Runner(BackendRegistry, Backend):

    registry_name = "dummy"
    extensible_path = "algorithms"
    # support_list = [DNNGenerator, SVMGenerator, KMeansGenerator]
    # support_list = {"dnn": DNNGenerator,
    #                 "svm": SVMGenerator,
    #                 "kmeans": KMeansGenerator}

    def __init__(self, algorithm, constraints, resources):
        # self.generator = support_list[algorithm]
        # if algorithm == "dnn":
        #     self.generator = DNNGenerator
        # elif algorithm == "svm":
        #     self.generator = SVMGenerator
        # elif algorithm == "kmeans":
        #     self.generator = KMeansGenerator

        self.constraints = constraints
        self.resources = resources

        path = os.path.dirname(os.path.abspath(__file__)) + "/"
        print(path)
        runner_spec=importlib.util.spec_from_file_location("runner", path + self.extensible_path + "/" + algorithm + "/runner.py")
        runner_module = importlib.util.module_from_spec(runner_spec)

        runner_spec.loader.exec_module(runner_module)
        #runner_module = importlib.import_module("homunculus.backends.taurus-bm."
        #                                        + "algorithms." + algorithm
        #                                        + ".runner")
        self.generator = getattr(runner_module, "Runner")

        return

    @classmethod
    def checkResources(cls, resources):
        return

    @classmethod
    def checkConstraints(cls, constraints):
        return

    # def getSupportedAlgorithms():
    #    algos = dir(algorithms)
    #    algos = list(filter(lambda elem: not(elem[0:2] == "__"), algos))
    #    return algos

    def getRegistryEntry():
        # entry = {}
        # entry["supported_algorithms"] = Dummy.getSupportedAlgorithms()
        # entry["check_resources"] = Dummy.checkResources
        # entry["check_constraints"] = Dummy.checkConstraints

        return

    def generate(self, model):
        self.generator.generate(model)

    def checkFeasibility(self, model):
        print("Checking feasibility...")
        return True
    
    def checkFeasibilityPar(self, models, print_summary=False, final=False):
        print("Generating Dummy Code for model set")

        valids = [True] * len(models)
        res_use = [(0,0)] * len(models)
        return valids, res_use
