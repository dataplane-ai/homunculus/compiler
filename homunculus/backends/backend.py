# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import abc


class Backend(abc.ABC):

    @classmethod
    @abc.abstractmethod
    def checkResources(resources):
        pass

    @classmethod
    @abc.abstractmethod
    def checkConstraints(constraints):
        pass

    @classmethod
    @abc.abstractmethod
    def generate(model):
        pass

    @classmethod
    @abc.abstractmethod
    def checkFeasibility(model):
        pass
