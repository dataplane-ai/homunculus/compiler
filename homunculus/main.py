# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

from .midend.metaparser import MetaParser
from .backends.registry import BackendRegistry
from .midend.algorithms.registry import AlgorithmRegistry
from .utils.registry import Registry as TR
from .midend.optimizer import Optimizer
import pprint
from colorama import init, Fore
import colored_traceback
import os
import sys
import importlib
from tensorflow.compat.v1 import logging as tf_log


class Homunculus():
    
    def __init__(self):

        self.h_state = { 
                "backends": {} 
        }

        return

    def installBackend(self, backend_path, platform):
        
        runner_spec = importlib.util.spec_from_file_location("runner", backend_path + "/runner.py")
        runner_module = importlib.util.module_from_spec(runner_spec)
        sys.modules["runner"] = runner_module
        runner_spec.loader.exec_module(runner_module)

        self.h_state["backends"][runner_module.Runner.registry_name] = runner_module 
        
        platform_spec = importlib.util.spec_from_file_location("platform", backend_path + "/platform.py")
        platform_module = importlib.util.module_from_spec(platform_spec)
        platform_spec.loader.exec_module(platform_module)
        
        return getattr(platform_module, platform) 

    def generate(self, platform, generate_metadata=True, filename_addition="", gen_dir="./"):

        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
        tf_log.set_verbosity(tf_log.ERROR)
        init()
        colored_traceback.add_hook(always=True)

        print(Fore.CYAN + "Generating Homunculus model")
        mp = MetaParser()

        program = None

        print("Processing metadata")
        if generate_metadata:
            print("  -> Creating metadata")
            mp.createMetadataFromPlatform(platform, filename_addition)

            print("  -> Reading metadata")
            program = mp.readMetadata()

        else:
            print("  -> Reading metadata")
            program = platform.getProgram()

        pp = pprint.PrettyPrinter(indent=4)
        registry = TR()
        print("Verifying program")
        registry.verify(program, self.h_state)

        print("Starting Optimizer")
        Optimizer.run(program)

        del registry

