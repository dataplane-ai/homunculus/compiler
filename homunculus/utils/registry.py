# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import os
import abc
import sys
import copy
import inspect
import importlib
from colorama import Fore


class MetaRegistry(abc.ABCMeta):

    registry = {}
    index_chain = []
    extensible_path = None
    support_list = None
    defaults_list = None

    def __new__(cls, name, bases, namespace):
        new_cls = super().__new__(cls, name, bases, namespace)
        
        if inspect.isabstract(new_cls):
            return new_cls

        if len(bases) == 0:
            if not(new_cls.defaults_list is None):
                cls.registry["defaults"] = copy.deepcopy(new_cls.defaults_list)
                new_cls.defaults_list = None

            return new_cls

        parent = bases[0]
        new_cls.index_chain = parent.index_chain + [new_cls.registry_name]
        sub_registry = cls.registry
        for entry in parent.index_chain:
            if entry in sub_registry:
                sub_registry = sub_registry[entry]
            elif (("supported" in sub_registry) and
                  (entry in sub_registry["supported"])):
                sub_registry = sub_registry["supported"][entry]
            else:
                raise Exception("Field: \"" + entry + "\" not found")

        if (("supported" in sub_registry) and
           (new_cls.registry_name in sub_registry["supported"])):
            sub_registry["supported"][new_cls.registry_name]["runner"] \
                         = new_cls
        else:
            sub_registry[new_cls.registry_name] = {}
            sub_registry[new_cls.registry_name]["class"] = new_cls
        support_path = ""

        if not(new_cls.extensible_path is None):
            support_path = os.path.dirname(os.path.abspath(
                                           sys.modules[new_cls.__module__]
                                           .__file__))
            support_path = support_path + "/" + new_cls.extensible_path
            new_cls.support_list = list(filter(
                                   lambda x: os.path.isdir(
                                    support_path + "/" + x)
                                   and not(x == "__pycache__"),
                                   os.listdir(support_path)))

        # if issubclass(new_cls, Support):

        #     if not("supported" in sub_registry):
        #         sub_registry["supported"] = {}

        #     sub_registry["supported"][new_cls.support_name] = new_cls

        if (("supported" in sub_registry) and
           (new_cls.registry_name in sub_registry["supported"])):
            sub_registry = sub_registry["supported"][new_cls.registry_name]
        else:
            sub_registry = sub_registry[new_cls.registry_name]

        if not(new_cls.support_list is None):
            sub_registry["ext_path"] = new_cls.extensible_path
            if not("supported" in sub_registry):
                sub_registry["supported"] = {}

            for support in new_cls.support_list:
                sub_registry["supported"][support] = {}

        sub_registry["defaults"] = new_cls.defaults_list

        return new_cls


class Registry(metaclass=MetaRegistry):

    defaults_list = {
            "algorithms": ["dnn"],
            "metrics": ["accuracy"]
    }

    @property
    def registry_name(self):
        return "registry"

    def __init__(self):
        self.registry = MetaRegistry.registry

    def isUndef(self, field):
        return field == "UNDEFINED"

    def setUndefs(self, program):
        print(Fore.YELLOW)

        if self.isUndef(program["platform name"]):
            raise Exception("Field: \"platform name\" cannot be undefined")

        for model_name, model in program["models"].items():

            registry_defaults = self.getDefaults()
            if self.isUndef(model["algorithm"]):
                print("Algorithms for model \"" + model_name +
                      "\" is undefined. Setting it to: " +
                      str(registry_defaults["algorithms"]))

                model["algorithm"] = registry_defaults["algorithms"]

            if self.isUndef(model["optimization_metric"]):
                print("Optimization metric for model \"" + model_name +
                      "\" is undefined. Setting it to: " +
                      str(registry_defaults["metrics"]))

                model["optimization_metric"] = registry_defaults["metrics"]

        print(Fore.CYAN)

    @classmethod
    def getDefaults(self):
        sub_registry = self.registry
        for entry in self.index_chain:
            sub_registry = sub_registry[entry]

        return sub_registry["defaults"]

    def checkSupport(self, path, support_list, prefix="", suffix=""):
        supported = self.get(path + "/supported")
        
        if not(isinstance(support_list, list)):
            support_list = [support_list]
        s = (set(support_list)).issubset(set(supported.keys()))
        if (s):
            return supported

        raise Exception(prefix + " \"" + str(support_list) +
                        "\" are unsupported by " + suffix)

    def addSupport(self, path, elem, cls):
        supported = self.get(path + "/supported")
        supported[elem] = {}
        
        sub_registry = supported[elem] = {}
        
        support_path = None
        if not(cls.extensible_path is None):
            support_path = os.path.dirname(os.path.abspath(
                                           sys.modules[cls.__module__]
                                           .__file__))
            support_path = support_path + "/" + cls.extensible_path
            cls.support_list = list(filter(
                                   lambda x: os.path.isdir(
                                    support_path + "/" + x)
                                   and not(x == "__pycache__"),
                                   os.listdir(support_path)))

        if not(cls.support_list is None):
            sub_registry["ext_path"] = cls.extensible_path
            if not("supported" in sub_registry):
                sub_registry["supported"] = {}

            for support in cls.support_list:
                sub_registry["supported"][support] = {}
                sub_registry["supported"][support]["path"] = support_path + "/" + support

        sub_registry["defaults"] = cls.defaults_list


        
        return

    def verify(self, program, h_state):
        self.setUndefs(program)
        self.checkPlatform(program, h_state)
        self.checkPlatformAlgorithms(program, h_state)
        self.checkOptimizerAlgorithms(program)

    @classmethod
    def get(self, path, prefix=""):
        path = path.replace("//", "/")
        keys = path.split("/")
        sub_registry = self.registry
        key_name = ""

        try:
            for key in keys:
                if key == "":
                    continue
                key_name = key
                sub_registry = sub_registry[key]

            return sub_registry

        except KeyError:
            raise Exception(prefix + " \"" + key_name + "\" not found")

    def checkPlatform(self, program, h_state):
        platform_name = program["platform name"]
        runner = None 
        if (platform_name in h_state["backends"]):
            runner_module = h_state["backends"][platform_name]
            runner = getattr(runner_module, "Runner")
            self.addSupport("backends/", platform_name, runner)
        
        be = self.checkSupport("backends/", platform_name, "Platform")
        # print("Platform " + platform_name + " found: " + str(be))
        print("Platform " + platform_name + " found")
        ext_path = self.get("backends/")["ext_path"].replace("/", "")
        platform_path = platform_name
        if not(ext_path == "."):
            platform_path = ext_path + "." + platform_name
        
        if not(platform_name in h_state["backends"]):
            runner_module = importlib.import_module("homunculus.backends." +
                                                platform_path + ".runner")
        
            runner = getattr(runner_module, "Runner")
        
        be[platform_name]["runner"] = runner
        
        print("Adding Runner module for platform: " + str(runner) + "\n")
        runner.checkConstraints(program["constraints"])
        runner.checkResources(program["resources"])

    def checkPlatformAlgorithms(self, program, h_state):
        
        platform_name = program["platform name"]
        platform_path = "backends/supported/" + platform_name
        for model_name, model in program["models"].items():
            plat = self.checkSupport(platform_path, model["algorithm"],
                                     "Algorithms", " platform \"" +
                                     platform_name + "\"")
            # print("Algorithm " + str(model["algorithm"]) + " found for platform: " + str(plat))
            print("Algorithm " + str(model["algorithm"]) + " found for platform: " + platform_name)

            ext_path = self.get(platform_path)["ext_path"].replace("/", "")
            for algo in model["algorithm"]:
                
                runner_module = None
                
                if ("path" in plat[algo]):
                    spec_path = plat[algo]["path"] + "/runner.py"
                    runner_spec = importlib.util.spec_from_file_location("runner", spec_path)
                    runner_module = importlib.util.module_from_spec(runner_spec)
                    sys.modules["runner"] = runner_module
                    runner_spec.loader.exec_module(runner_module)
 
                else: 
                    algo_path = platform_name + "." + algo
                    if not(ext_path == "."):
                        algo_path = platform_name + "." + ext_path + "." + algo
                    runner_module = importlib.import_module("homunculus.backends."
                                                            + algo_path +
                                                            ".runner")
                runner = getattr(runner_module, "Runner")
                print("Adding Runner module for platform algorithm: " + str(runner) + "\n")
                plat[algo]["runner"] = runner

    def checkOptimizerAlgorithms(self, program):
        for model_name, model in program["models"].items():
            for algo in model["algorithm"]:
                algo_reg = self.checkSupport("algorithms/", algo,
                                             "Algorithms", " the Homunculus " +
                                             "optimizer")
                

                # print("Algorithm " + str(model["algorithm"]) + " found for Optimizer : " + str(algo_reg))
                print("Algorithm " + str(model["algorithm"]) + " found for Optimizer")
                ext_path = self.get("algorithms/")["ext_path"].replace("/", "")
                algo_path = "algorithms." + algo
                if not(ext_path == "."):
                    algo_path = "algorithms" + "." + ext_path + "." + algo

                runner_module = importlib.import_module("homunculus.midend." +
                                                        algo_path + ".runner")
                runner = getattr(runner_module, "Runner")
                print("Adding Runner module for optimizer algorithm: " + str(runner) + "\n")
                algo_reg[algo]["runner"] = runner
