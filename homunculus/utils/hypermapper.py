# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import csv
import os
from subprocess import Popen, PIPE
from multiprocessing import Pool
from itertools import zip_longest


def csHypermapper(scenario, io_dict, func, func_state):

    func_state["optimum"]["metric_value"] = 0
    func_state["optimum"]["arch"] = None
    func_state["optimum"]["model"] = None
    func_state["optimum"]["pars"] = None

    func_headers = ""
    for key in io_dict["inputs"]:
        func_headers += key + ","
    for key in io_dict["outputs"]:
        func_headers += key + ","
    func_headers += "Valid\n"
    print(func_headers)

    # cmd = ["python", os.environ["HYPERMAPPER_HOME"] +
    #       "/scripts/hypermapper.py", scenario]
    cmd = ["hypermapper", scenario]
    print(cmd)
    p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding="utf-8")
    i = 0

    while p.poll() is None:
        p.stdout.flush()
        request = p.stdout.readline()
        p.stdout.flush()

        if "End of HyperMapper" in request:
            print("HyperMapper optimization finished")
            break

        print("HyperMapper Iteration: " + str(i))
        print(request)
        num_of_eval_requests = int(request.split(' ')[1])
        headers = p.stdout.readline()
        p.stdin.flush()
        headers = [h.strip() for h in headers.split(",")]

        parameter_list = []
        response_list = []
        for row in range(num_of_eval_requests):
            parameters = p.stdout.readline().strip() + ","
            response_list.append(parameters)
            parameters = [x.strip() for x in parameters.split(',')]
            parameter_dict = dict(zip(headers, parameters))
            parameter_list.append(parameter_dict)

        multi_out = []
        mp_params = None
        mp_params = list(zip_longest(parameter_list, [func_state],
                                     fillvalue=func_state))
        # with Pool(num_of_eval_requests) as worker:
        #    multi_out = worker.map(func, enumerate(mp_params))
        # for idx, param_set in enumerate(mp_params):
        for param_set in parameter_list:
            print(param_set)
            multi_out.append(func((param_set, func_state)))

        models = []
        for idx, res in enumerate(multi_out):
            *output, arch, layers, pars = res
            for elem in output:
                response_list[idx] += (str(elem) + ",")

            models.append({
                "name": (func_state["name"] + "_" +
                         str(i) + "_" + str(idx)),
                "model": layers,
                "arch": arch,
                "pars": pars,
                "input_dim": func_state["input_dim"],
                "output_dim": func_state["output_dim"]
            })

            print("Arch: " + str(arch))
            print("Metric: " + str(output))
            # print("Layers: " + str(layers))

        valids, res_use = func_state["backend"].checkFeasibilityPar(models)
        response = func_headers
        for idx, res in enumerate(multi_out):
            *output, arch, layers, pars = res
            response_list[idx] += (str(valids[idx]) + "\n")
            response += response_list[idx]

            app_valid = False
            if valids[idx] == "True":
                app_valid = True
            optimum = app_valid and ((-1.0 * output[0]) >
                                     func_state["optimum"]["metric_value"])

            if optimum:
                func_state["optimum"]["metric_value"] = -1.0 * output[0]
                func_state["optimum"]["arch"] = arch
                func_state["optimum"]["model"] = layers
                func_state["optimum"]["pars"] = pars

        print("Current optimum: " + str(func_state["optimum"]["metric_value"]))

        plotBO(func_state["name"], func_state["optimum"]["metric_value"], res_use, i)

        p.stdin.write(response)
        p.stdin.flush()
        i += 1


def plotBO(app_name, optimum, res_use, idx):

    filename = os.path.splitext(sys.argv[0])[0]
    plots_dir = filename + "_plots/"
    os.makedirs(plots_dir, exist_ok=True)
    plot_csv = app_name + ".csv"
    plots_file = plots_dir + plot_csv

    with open(plots_file, 'a+') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=",", quotechar='|',
                                quoting=csv.QUOTE_MINIMAL)

        # row = ["iter", "iter_value", "optimum_value", "valid", "doe"]
        if idx == 0:
            row = ["app_name", "iter", "optimum", "PCUs", "PMUs"]
            spamwriter.writerow(row)
        # plot = state["plot"]
        # for idx, iter_val in enumerate(plot["iter_value"]):
        # str(plot["valid"][idx]), str(plot["doe"][idx])]
        row = [app_name, str(idx), str(optimum), str(res_use[0]),
               str(res_use[1])]
        spamwriter.writerow(row)

