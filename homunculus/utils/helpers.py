# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

from subprocess import Popen, PIPE
from itertools import zip_longest
from multiprocessing import Pool
import csv
import sys
import os


def write2DCSV(filename, matrix2D):

    with open(filename, 'w+') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=",", quotechar='|',
                                quoting=csv.QUOTE_MINIMAL)
        for arr in matrix2D:
            row = []
            for elem in arr:
                row.append(str(elem))

            spamwriter.writerow(row)


def makeLUTsFromModel(layers, lut_dir):

    for idx, layer_params in enumerate(layers):
        # layer_params = layer.get_weights()
        # print(layer_params)
        if len(layer_params[1]) == 0:
            # this must be a layer with 0 neurons
            # so we check 0 size bias layer
            continue
        makeDNNWeightFile(layer_params, lut_dir, "L" + str(idx))
        makeDNNBiasFile(layer_params, lut_dir, "L" + str(idx))


def makeDNNWeightFile(layer_params, lut_dir, prefix):

    weight_file = lut_dir + "/" + prefix + "_NEURON_W_LUT" + ".csv"
    weights = layer_params[0]
    weights = weights.transpose([1, 0])
    write2DCSV(weight_file, weights)

    return


def makeDNNBiasFile(layer_params, lut_dir, prefix):

    bias_file = lut_dir + "/" + prefix + "_NEURON_B_LUT" + ".csv"
    bias = layer_params[1]
    bias = bias.reshape(-1, len(bias)).transpose([1, 0])
    write2DCSV(bias_file, bias)


def setupGenDir(base_dir, app_name):

    app_dir = base_dir + "/" + app_name
    os.makedirs(app_dir, exist_ok=True)

    lut_dir = app_dir + "/" + "luts"
    os.makedirs(lut_dir, exist_ok=True)

    return app_dir


class Spatial:

    def __init__(self):
        self.spatial_str = ""
        self.indent = 0

    def getSpatialStr(self):
        return self.spatial_str

    def addIndent(self, line):
        for i in range(self.indent):
            line = "\t" + line
        return line

    def addNewline(self, line=None):
        if line is None:
            self.spatial_str += "\n"
        else:
            line += "\n"
        return line

    def format(self, line):
        return self.addNewline(self.addIndent(line))

    def loopBody(self, lines):
        for idx, line in enumerate(lines):
            lines[idx] = "\t" + line
        return lines

    def writeBlock(self, lines):
        for line in lines:
            self.spatial_str += self.format(line)
        self.addNewline()

    def incIndentation(self):
        self.indent += 1

    def decIndentation(self):
        self.indent -= 1

    def makeSpatialFile(self, base_dir, app_name):
        with open(base_dir + "/" + app_name + ".scala", "w") as fp:
            fp.write(self.spatial_str)

    def setupPackages(self, *args):
        lines = []
        for arg in args:
            lines.append("package " + arg)
        self.writeBlock(lines)

    def setupImports(self, *args):
        lines = []
        for arg in args:
            lines.append("import " + arg)
        self.writeBlock(lines)

    def setupClass(self, app_name):
        lines = []
        lines.append("@spatial class " + app_name +
                     " extends SpatialTest {")
        self.writeBlock(lines)
        self.incIndentation()

    def setupKernelTrait(self, lib_file):
        lines = []
        lines.append("@spatial trait Kernel extends SpatialApp with " + lib_file + " {")
        self.writeBlock(lines)
        self.incIndentation()

    def setupConstants(self, constants):
        lines = []
        for key, val in constants.items():
            lines.append("val " + key + " = " + str(val))
        self.writeBlock(lines)

    def setupTypes(self, types):
        lines = []
        for key, val in types.items():
            lines.append("type " + key + " = " + str(val))
        self.writeBlock(lines)

    def setupInOutFile(self):
        lines = []
        lines.append("val infile = buildPath(IR.config.genDir," +
                     "\"tungsten\", \"in.csv\")")
        lines.append("val outfile = buildPath(IR.config.genDir," +
                     "\"tungsten\", \"out.csv\")")
        lines.append("createDirectories(dirName(infile))")
        lines.append("val inputs = List.tabulate(N * field) {i => i}")
        lines.append("writeCSVNow(inputs, infile)")
        self.writeBlock(lines)

    def setupStreams(self):
        lines = []
        lines.append("val stream_in  = StreamIn[T]" +
                     "(FileBus[T](infile))")
        lines.append("val stream_out  = StreamOut[Tup2[I32,Bit]]" +
                     "(FileEOFBus[Tup2[I32,Bit]](outfile))")
        self.writeBlock(lines)

    def setupMain(self):
        self.writeBlock(["def main(args: Array[String]): Unit = {"])
        self.incIndentation()
        self.setupInOutFile()
        self.setupStreams()
        self.writeBlock(["Accel {"])
        self.incIndentation()

    def setupMemories(self, mems):
        lines = []
        for key, val in mems.items():
            line = ("val " + key + " = " +
                    val["mem_type"] + "[" +
                    val["data_type"] + "](" +
                    str(val["dims"])[1:-1] + ")")
            if (val["mem_type"] == "FileLUT") or \
               (val["mem_type"] == "LUT.fromFile"):
                line += "(" + val["path"] + ")"
            lines.append(line)

        self.writeBlock(lines)

    def packetInput(self):
        self.writeBlock(["Pipe {",
                        "\tval packet = inbus.value",
                         "\tpacket_use1.enq(packet)",
                         "\tpacket_use2.enq(packet)",
                         "}"])

    def packetOutput(self, stage, offset):
        self.writeBlock(["Pipe {",
                        "\tval decision = dummy_stage" + str(stage) +
                         "_fifo.deq()",
                         "\tval packet = packet_use2.deq()",
                         "\tval newPacket = AxiStream512((" +
                         "packet.tdata.as[U512]) | (decision.as[U512] << " +
                         str(offset) + "), packet.tstrb, packet.tkeep, " +
                         "packet.tlast, packet.tid, 1, 0)",
                         "\toutbus := newPacket",
                         "}"])

    def parsePacket(self):
        self.writeBlock(["Pipe {",
                        "\tval packet = packet_use1.deq()",
                         "\tval eth = 112",
                         "\tval ip = 160",
                         "\tval shift_amounts = Seq.tabulate(num_fields){ " +
                         "i => (eth + ip + (i * 32)).to[I16]}",
                         "\tForeach(0 until num_fields par num_fields){ i =>",
                         "\t\tval mux1H_conds = Seq.tabulate(num_fields){" +
                         "j => j.to[I32] === i}",
                         "\t\tval shifted_pkt = oneHotMux(mux1H_conds, " +
                         "shift_amounts.map{amt => packet.tdata.as[U512] " +
                         ">> amt})",
                         "\t\tinput(i) = cat(shifted_pkt.bits(7::0), " +
                         "shifted_pkt.bits(15::8), shifted_pkt.bits(23::16)"
                         ", shifted_pkt.bits(31::24)).as[T]",
                         "\t}",
                         "\tdummy_stageinput_fifo.enq(input(num_fields - 1))",
                         "}"])

    def parsePacketSRAM(self, typ, width, offset):

        self.writeBlock([
            "// Read the input features into SRAM",
            "Pipe {",
            "\tForeach(0 until NUM_INPUT_FEATURES){ i =>",
            "\t\t// Calculate index of next feature and read it into SRAM",
            "\t\tval idx = (i * " + str(width) + ") + " + str(offset),
            "\t\tinputs(i) = readBytesToWord[U8](pkt, idx, " + str(width) + ").to[" + typ + "]"
            "\t}",
            "}",
        ])


    def dummyEnqFifo(self, stage, elem):
        self.writeBlock(["dummy_stage" + str(stage) + "_fifo.enq(" + elem +
                         ")"])

    def dummyDeqFifo(self, stage):
        self.writeBlock(["val dummy = dummy_stage" + str(stage) +
                         "_fifo.deq()"])

    def buildDNNLayerUnrolled(self, dim0, dim1, idx, arg_mem, act=True):

        act_line = None
        if act:
            act_line = "\tL" + str(idx) + "_res(i) = max(w + L" + str(idx) + \
                       "_B_LUT(i), 0)"
        else:
            act_line = "\tL" + str(idx) + "_res(i) = w + L" + str(idx) + \
                       "_B_LUT(i)"

        self.writeBlock([
                        "List.tabulate(" + dim0 + ") { i =>",
                        "\tval partial_results = List.tabulate(" + dim1 +
                        ") { j =>",
                        "\t\tL" + str(idx) + "_W_LUT(i, j) * " + arg_mem +
                        "(j)",
                        "\t}",
                        "\tval w = partial_results.reduceTree {_+_}",
                        act_line,
                        "}"])
    
    def setupDimList(self, dims):
        dim_str = "List("
        for i in dims:
            dim_str = dim_str + str(i) + ","

        return [dim_str[:-1] + ")"]

    def setupLUTList(self, num_layers, lut_dir):
        lut_str = "List(\n"

        for i in range(num_layers):
            lut_str = lut_str + "( " + lut_dir + " + \"L" + str(i) + "_W.csv, " + lut_dir + " + /L" + str(i) + "_B.csv ),\n"

        lut_str = lut_str[:-2] + "\n)"
        return lut_str

    def pipeStage(self, lines):
        
        self.writeBlock(["Pipe {"])
        self.incIndentation()
        self.writeBlock(lines)
        self.decIndentation()
        self.writeBlock(["}"])

    def startStream(self, num_iters, idx):
        self.writeBlock(["Stream.Foreach(N by 1) { " + idx + "=>"])
        #self.writeBlock(["Foreach(*) { " +
        #                idx + "=>"])

        self.incIndentation()

    def foreach(self, num_iters, par, idx, inner_lines, write=True):
        lines = []
        lines.append("Foreach(0 until " + str(num_iters) + " par " +
                     str(par) + ") { " + str(idx) + " =>")
        lines.extend(self.loopBody(inner_lines))
        lines.append("}")

        if write is True:
            self.writeBlock(lines)
        else:
            return lines

    def reduce(self, type, num_iters, par, idx, out,
               inner_lines,
               reduce_lines,
               write=True):

        lines = []
        lines.append("val " + out + " = Reduce(Reg[" + type + "])(" +
                     str(num_iters) + " par " + str(par) + "){ " + idx + " =>")

        lines.extend(self.loopBody(inner_lines))
        lines.append("}{" + reduce_lines + "}")

        if write is True:
            self.writeBlock(lines)
        else:
            return lines

    def buildReLU(self, out, weights, bias_lut):
        line = out + " = " + "max(" + weights + " + " + bias_lut + ", 0)"
        return [line]

    def buildDNNLayer(self, dict):

        inner_line = dict["weight_lut"] + "(i,j) * " + dict["arg_mem"] + "(j)"

        red = self.reduce(dict["type"], dict["neuron_size"],
                          dict["inner_par"], "j", "w", [inner_line],
                          "_ + _", False)

        red.extend(self.buildReLU(dict["result_mem"] + "(i)", "w",
                                  dict["bias_lut"] + "(i)"))

        self.foreach(dict["num_neurons"], dict["outer_par"], "i", red)

    def chooseMultiClassResult(self, type, num_classes, par, arg_mem, out):

        self.reduce(type, num_classes, par, "i", "maxVal", [arg_mem + "(i)"],
                    "(a,b) => max(a,b)")

        self.reduce("I32", num_classes, par, "i", out,
                    ["mux(" + arg_mem + "(i) == maxVal.value, i, -1)"],
                    "(a,b) => max(a,b)")

    def endStream(self, out, idx):
        self.writeBlock(["stream_out := Tup2(" + out + ".value, " +
                        idx + " == (N-1))"])
        self.decIndentation()
        self.writeBlock(["}"])
        self.decIndentation()

    def closeBraces(self):
        self.writeBlock(["}", "assert(true)"])
        self.decIndentation()
        self.writeBlock(["}"])
        self.decIndentation()
        self.writeBlock(["}"])


def csHypermapper(scenario, io_dict, func, func_state):

    func_state["optimum"]["metric_value"] = 0
    func_state["optimum"]["arch"] = None
    func_state["optimum"]["model"] = None
    func_state["optimum"]["pars"] = None

    func_headers = ""
    for key in io_dict["inputs"]:
        func_headers += key + ","
    for key in io_dict["outputs"]:
        func_headers += key + ","
    func_headers += "Valid\n"
    print(func_headers)

    # cmd = ["python", os.environ["HYPERMAPPER_HOME"] +
    #        "/scripts/hypermapper.py", scenario]
    cmd = ["hypermapper", scenario]
    # print(cmd)
    p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding="utf-8")
    i = 0

    while p.poll() is None:
        p.stdout.flush()
        request = p.stdout.readline()
        p.stdout.flush()

        if "End of HyperMapper" in request:
            print("HyperMapper optimization finished")
            break

        print("HyperMapper Iteration: " + str(i))
        print("HM Request: " + request)
        if request.startswith("warning"):
            continue
        
        #print(len(request.split(' ')))
        if len(request.split(' '))  < 2:
            continue    

        num_of_eval_requests = int(request.split(' ')[1])
        headers = p.stdout.readline()
        p.stdin.flush()
        headers = [h.strip() for h in headers.split(",")]

        parameter_list = []
        response_list = []
        for row in range(num_of_eval_requests):
            parameters = p.stdout.readline().strip() + ","
            response_list.append(parameters)
            parameters = [x.strip() for x in parameters.split(',')]
            parameter_dict = dict(zip(headers, parameters))
            parameter_list.append(parameter_dict)

        multi_out = []
        mp_params = None
        mp_params = list(zip_longest(parameter_list, [func_state],
                                     fillvalue=func_state))
        # with Pool(num_of_eval_requests) as worker:
        #    multi_out = worker.map(func, enumerate(mp_params))
        # for idx, param_set in enumerate(mp_params):
        for param_set in parameter_list:
            # print(param_set)
            multi_out.append(func((param_set, func_state)))

        models = []
        for idx, res in enumerate(multi_out):
            *output, arch, layers, pars = res
            for elem in output:
                response_list[idx] += (str(elem) + ",")

            models.append({
                "name": (func_state["name"] + "_" +
                         str(i) + "_" + str(idx)),
                "model": layers,
                "arch": arch,
                "pars": pars,
                "input_dim": func_state["input_dim"],
                "output_dim": func_state["output_dim"]
            })

            print("Arch: " + str(arch))
            print("Metric: " + str(output))
            # print("Layers: " + str(layers))

        valids, res_use = func_state["backend"].checkFeasibilityPar(models)
        response = func_headers
        for idx, res in enumerate(multi_out):
            *output, arch, layers, pars = res
            response_list[idx] += (str(valids[idx]) + "\n")
            response += response_list[idx]
            
            app_valid = False
            if valids[idx] == True:
                app_valid = True

            # print("----+++++++++++++++++++++++++++++++----")
            print("Model score: " + str(-1.0 * output[0]))
            # print(func_state["optimum"]["metric_value"])
            # print(app_valid)
            optimum = app_valid and ((-1.0 * output[0]) >
                                     func_state["optimum"]["metric_value"])
            # print(optimum)
            # print("----+++++++++++++++++++++++++++++++----")

            if optimum:
                func_state["optimum"]["metric_value"] = -1.0 * output[0]
                func_state["optimum"]["arch"] = arch
                func_state["optimum"]["model"] = layers
                func_state["optimum"]["pars"] = pars
                func_state["optimum"]["res_use"] = res_use[idx]

        print("Current optimum: " + str(func_state["optimum"]["metric_value"]))

        plotBO(func_state["name"], func_state["optimum"]["metric_value"], func_state["optimum"]["res_use"], i)

        p.stdin.write(response)
        p.stdin.flush()
        i += 1


def plotBO(app_name, optimum, res_use, idx):

    filename = os.path.splitext(sys.argv[0])[0]
    plots_dir = filename + "_plots/"
    os.makedirs(plots_dir, exist_ok=True)
    plot_csv = app_name + ".csv"
    plots_file = plots_dir + plot_csv

    with open(plots_file, 'a+') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=",", quotechar='|',
                                quoting=csv.QUOTE_MINIMAL)

        # row = ["iter", "iter_value", "optimum_value", "valid", "doe"]
        if idx == 0:
            row = ["app_name", "iter", "optimum", "PCUs", "PMUs"]
            spamwriter.writerow(row)
        # plot = state["plot"]
        # for idx, iter_val in enumerate(plot["iter_value"]):
        # str(plot["valid"][idx]), str(plot["doe"][idx])]
        row = [app_name, str(idx), str(optimum), str(res_use[0]),
               str(res_use[1])]
        spamwriter.writerow(row)

