# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import csv
import os


def write2DCSV(filename, matrix2D):

    with open(filename, 'w+') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=",", quotechar='|',
                                quoting=csv.QUOTE_MINIMAL)
        for arr in matrix2D:
            row = []
            for elem in arr:
                row.append(str(elem))

            spamwriter.writerow(row)


def makeLUTsFromModel(layers, lut_dir):

    for idx, layer_params in enumerate(layers):
        # layer_params = layer.get_weights()
        makeDNNWeightFile(layer_params, lut_dir, "L" + str(idx))
        makeDNNBiasFile(layer_params, lut_dir, "L" + str(idx))


def makeDNNWeightFile(layer_params, lut_dir, prefix):

    weight_file = lut_dir + "/" + prefix + "_NEURON_W_LUT" + ".csv"
    weights = layer_params[0]
    weights = weights.transpose([1, 0])
    write2DCSV(weight_file, weights)

    return


def makeDNNBiasFile(layer_params, lut_dir, prefix):

    bias_file = lut_dir + "/" + prefix + "_NEURON_B_LUT" + ".csv"
    bias = layer_params[1]
    bias = bias.reshape(-1, len(bias)).transpose([1, 0])
    write2DCSV(bias_file, bias)


def setupGenDir(base_dir, app_name):

    app_dir = base_dir + "/" + app_name
    os.makedirs(app_dir, exist_ok=True)

    lut_dir = app_dir + "/" + "luts"
    os.makedirs(lut_dir, exist_ok=True)

    return app_dir
