# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

class Spatial:

    def __init__(self):
        self.spatial_str = ""
        self.indent = 0

    def getSpatialStr(self):
        return self.spatial_str

    def addIndent(self, line):
        for i in range(self.indent):
            line = "\t" + line
        return line

    def addNewline(self, line=None):
        if line is None:
            self.spatial_str += "\n"
        else:
            line += "\n"
        return line

    def format(self, line):
        return self.addNewline(self.addIndent(line))

    def loopBody(self, lines):
        for idx, line in enumerate(lines):
            lines[idx] = "\t" + line
        return lines

    def writeBlock(self, lines):
        for line in lines:
            self.spatial_str += self.format(line)
        self.addNewline()

    def incIndentation(self):
        self.indent += 1

    def decIndentation(self):
        self.indent -= 1

    def makeSpatialFile(self, base_dir, app_name):
        with open(base_dir + "/" + app_name + ".scala", "w") as fp:
            fp.write(self.spatial_str)

    def setupPackages(self, *args):
        lines = []
        for arg in args:
            lines.append("package " + arg)
        self.writeBlock(lines)

    def setupImports(self, *args):
        lines = []
        for arg in args:
            lines.append("import " + arg)
        self.writeBlock(lines)

    def setupClass(self, app_name):
        lines = []
        lines.append("@spatial class " + app_name +
                     " extends SpatialTest {")
        self.writeBlock(lines)
        self.incIndentation()

    def setupKernelTrait(self, lib_file):
        lines = []
        lines.append("@spatial trait Kernel extends SpatialApp with " + lib_file + " {")
        self.writeBlock(lines)
        self.incIndentation()

    def setupConstants(self, constants):
        lines = []
        for key, val in constants.items():
            lines.append("val " + key + " = " + str(val))
        self.writeBlock(lines)

    def setupTypes(self, types):
        lines = []
        for key, val in types.items():
            lines.append("type " + key + " = " + str(val))
        self.writeBlock(lines)

    def setupInOutFile(self):
        lines = []
        lines.append("val infile = buildPath(IR.config.genDir," +
                     "\"tungsten\", \"in.csv\")")
        lines.append("val outfile = buildPath(IR.config.genDir," +
                     "\"tungsten\", \"out.csv\")")
        lines.append("createDirectories(dirName(infile))")
        lines.append("val inputs = List.tabulate(N * field) {i => i}")
        lines.append("writeCSVNow(inputs, infile)")
        self.writeBlock(lines)

    def setupStreams(self):
        lines = []
        lines.append("val stream_in  = StreamIn[T]" +
                     "(FileBus[T](infile))")
        lines.append("val stream_out  = StreamOut[Tup2[I32,Bit]]" +
                     "(FileEOFBus[Tup2[I32,Bit]](outfile))")
        self.writeBlock(lines)

    def setupMain(self):
        self.writeBlock(["def main(args: Array[String]): Unit = {"])
        self.incIndentation()
        self.setupInOutFile()
        self.setupStreams()
        self.writeBlock(["Accel {"])
        self.incIndentation()

    def setupMemories(self, mems):
        lines = []
        for key, val in mems.items():
            line = ("val " + key + " = " +
                    val["mem_type"] + "[" +
                    val["data_type"] + "](" +
                    str(val["dims"])[1:-1] + ")")
            if (val["mem_type"] == "FileLUT") or \
               (val["mem_type"] == "LUT.fromFile"):
                line += "(" + val["path"] + ")"
            lines.append(line)

        self.writeBlock(lines)

    def packetInput(self):
        self.writeBlock(["Pipe {",
                        "\tval packet = inbus.value",
                         "\tpacket_use1.enq(packet)",
                         "\tpacket_use2.enq(packet)",
                         "}"])

    def packetOutput(self, stage, offset):
        self.writeBlock(["Pipe {",
                        "\tval decision = dummy_stage" + str(stage) +
                         "_fifo.deq()",
                         "\tval packet = packet_use2.deq()",
                         "\tval newPacket = AxiStream512((" +
                         "packet.tdata.as[U512]) | (decision.as[U512] << " +
                         str(offset) + "), packet.tstrb, packet.tkeep, " +
                         "packet.tlast, packet.tid, 1, 0)",
                         "\toutbus := newPacket",
                         "}"])

    def parsePacket(self):
        self.writeBlock(["Pipe {",
                        "\tval packet = packet_use1.deq()",
                         "\tval eth = 112",
                         "\tval ip = 160",
                         "\tval shift_amounts = Seq.tabulate(num_fields){ " +
                         "i => (eth + ip + (i * 32)).to[I16]}",
                         "\tForeach(0 until num_fields par num_fields){ i =>",
                         "\t\tval mux1H_conds = Seq.tabulate(num_fields){" +
                         "j => j.to[I32] === i}",
                         "\t\tval shifted_pkt = oneHotMux(mux1H_conds, " +
                         "shift_amounts.map{amt => packet.tdata.as[U512] " +
                         ">> amt})",
                         "\t\tinput(i) = cat(shifted_pkt.bits(7::0), " +
                         "shifted_pkt.bits(15::8), shifted_pkt.bits(23::16)"
                         ", shifted_pkt.bits(31::24)).as[T]",
                         "\t}",
                         "\tdummy_stageinput_fifo.enq(input(num_fields - 1))",
                         "}"])

    def parsePacketSRAM(self, typ, width, offset):

        self.writeBlock([
            "// Read the input features into SRAM",
            "Pipe {",
            "\tForeach(0 until NUM_INPUT_FEATURES){ i =>",
            "\t\t// Calculate index of next feature and read it into SRAM",
            "\t\tval idx = (i * " + str(width) + ") + " + str(offset),
            "\t\tinputs(i) = readBytesToWord[U8](pkt, idx, " + str(width) + ").to[" + typ + "]"
            "\t}",
            "}",
        ])


    def dummyEnqFifo(self, stage, elem):
        self.writeBlock(["dummy_stage" + str(stage) + "_fifo.enq(" + elem +
                         ")"])

    def dummyDeqFifo(self, stage):
        self.writeBlock(["val dummy = dummy_stage" + str(stage) +
                         "_fifo.deq()"])

    def buildDNNLayerUnrolled(self, dim0, dim1, idx, arg_mem, act=True):

        act_line = None
        if act:
            act_line = "\tL" + str(idx) + "_res(i) = max(w + L" + str(idx) + \
                       "_B_LUT(i), 0)"
        else:
            act_line = "\tL" + str(idx) + "_res(i) = w + L" + str(idx) + \
                       "_B_LUT(i)"

        self.writeBlock([
                        "List.tabulate(" + dim0 + ") { i =>",
                        "\tval partial_results = List.tabulate(" + dim1 +
                        ") { j =>",
                        "\t\tL" + str(idx) + "_W_LUT(i, j) * " + arg_mem +
                        "(j)",
                        "\t}",
                        "\tval w = partial_results.reduceTree {_+_}",
                        act_line,
                        "}"])
    
    def buildDimList(self, dims):
        dim_str = "List("
        for i in dims:
            dim_str = dim_str + str(i) + ","

        return [dim_str[:-1] + ")"]

    def setupLUTList(self, num_layers, lut_dir)
        lut_str = "List(\n"

        for i in range(num_layers):
            lut_str = lut_str + "( " + lut_dir + " + \"L" + str(i) + "_W.csv, " + lut_dir + " + /L" + str(i) + "_B.csv ),\n"

        lut_str = lut_str[:-2] + "\n)"
        return lut_str

    def pipeStage(self, lines):
        
        self.writeBlock(["Pipe {"])
        self.incIndentation()
        self.writeBlock(lines)
        self.decIndentation()
        self.writeBlock(["}"])

    def startStream(self, num_iters, idx):
        self.writeBlock(["Foreach(*) { " +
                        idx + "=>"])

        self.incIndentation()

    def foreach(self, num_iters, par, idx, inner_lines, write=True):
        lines = []
        lines.append("Foreach(0 until " + str(num_iters) + " par " +
                     str(par) + ") { " + str(idx) + " =>")
        lines.extend(self.loopBody(inner_lines))
        lines.append("}")

        if write is True:
            self.writeBlock(lines)
        else:
            return lines

    def reduce(self, type, num_iters, par, idx, out,
               inner_lines,
               reduce_lines,
               write=True):

        lines = []
        lines.append("val " + out + " = Reduce(Reg[" + type + "])(" +
                     str(num_iters) + " par " + str(par) + "){ " + idx + " =>")

        lines.extend(self.loopBody(inner_lines))
        lines.append("}{" + reduce_lines + "}")

        if write is True:
            self.writeBlock(lines)
        else:
            return lines

    def buildReLU(self, out, weights, bias_lut):
        line = out + " = " + "max(" + weights + " + " + bias_lut + ", 0)"
        return [line]

    def buildDNNLayer(self, dict):

        inner_line = dict["weight_lut"] + "(i,j) * " + dict["arg_mem"] + "(j)"

        red = self.reduce(dict["type"], dict["neuron_size"],
                          dict["inner_par"], "j", "w", [inner_line],
                          "_ + _", False)

        red.extend(self.buildReLU(dict["result_mem"] + "(i)", "w",
                                  dict["bias_lut"] + "(i)"))

        self.foreach(dict["num_neurons"], dict["outer_par"], "i", red)

    def chooseMultiClassResult(self, type, num_classes, par, arg_mem, out):

        self.reduce(type, num_classes, par, "i", "maxVal", [arg_mem + "(i)"],
                    "(a,b) => max(a,b)")

        self.reduce("I32", num_classes, par, "i", out,
                    ["mux(" + arg_mem + "(i) == maxVal.value, i, -1)"],
                    "(a,b) => max(a,b)")

    def endStream(self, out, idx):
        self.writeBlock(["stream_out := Tup2(" + out + ".value, " +
                        idx + " == (N-1))"])
        self.decIndentation()
        self.writeBlock(["}"])
        self.decIndentation()

    def closeBraces(self):
        self.writeBlock(["}", "assert(true)"])
        self.decIndentation()
        self.writeBlock(["}"])
        self.decIndentation()
        self.writeBlock(["}"])
