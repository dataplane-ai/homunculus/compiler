# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

from setuptools import setup, find_packages

#######################################################################################################################################################
# This is a fix if you get the following error
#
# [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: unable to get local issuer certificate (_ssl.c:1076) -- Some packages may not be found!
# Solved here (https://github.com/prisma-labs/python-graphql-client/issues/13)

import urllib.request
import ssl

ssl._create_default_https_context = ssl._create_unverified_context
response = urllib.request.urlopen('https://www.python.org')
print(response.read().decode('utf-8'))
#######################################################################################################################################################


setup(
    name='homunculus',
    version='1',
    description='A useful module',
    author='Tushar Swamy',
    author_email='tswamy@stanford.edu',
    packages=find_packages(include=['homunculus', 'homunculus.*']),
    setup_requires=[
        "cython"
    ],
    include_package_data=True,
    install_requires=[
	    "tabulate",
        "cython",
        "numpy==1.23.0",
        "scipy",
        'tensorflow-addons[tensorflow]',
        'tensorflow',
        'mkl',
        'networkx==2.5',
        'dill==0.3.3',
        'colorama==0.4.4',
        'colored_traceback==0.3.0',
        'scikit_learn',
        'tqdm==4.62.3',
        'hypermapper'
    ]  # external packages as dependencies
)
