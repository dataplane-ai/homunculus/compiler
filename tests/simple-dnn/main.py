# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import json
import csv
from homunculus import Homunculus
from homunculus.alchemy import DataLoader, Model
from homunculus.backends import Dummy


def load_from_file(dataset):
    full_data = []
    full_labels = []
    with open(dataset, newline='\n') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        for row in spamreader:
            label = []
            label.append(row.pop(-2))
            label.append(row.pop(-1))
            full_labels.append(label)
            full_data.append(row)

    return full_data, full_labels


@DataLoader
def wrapper_func():
    tnx, tny = load_from_file("train_ad.csv")
    tsx, tsy = load_from_file("test_ad.csv")
    return {
        "data": {
                "train": tnx,
                "test": tsx
        },
        "labels": {
            "train": tny,
            "test": tsy
        }
    }


def load_config(config_file):
    with open(config_file, 'r') as cf:
        return json.load(cf)


ad = Model({
    "algorithm": ["dnn"],
    "optimization_metric": ["f1"],
    "name": "example",
    "data_loader": wrapper_func,
    "config": {"dnn": load_config("config.json")}
})


h = Homunculus()
platform = Dummy()
platform.schedule(ad)

h.generate(platform)
